# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to install application? ###

* Download and install node.js
* Download this repository
* Open cmd in this folder
* Run commands:
	* npm install
	* npm start
* If "npm start" doesn't work, try to type "npm serve"

### How does slider work ###

Slider component is called SliderContainer which contains SliderFrame, Buttons and Dots.
SliderFrame is a component which is responsible for smooth Slides movements.
Slides are spawning and deleting in SliderFrame depending on the current slide.

### Features ###

* Each slide can contain any HTML content
* Slider is infinite
* Slide can be changed to any other slide by pressing one of the dots below
* Some parameters are customizable
* There are no glitces when user rapidly presses left or right button