import React from 'react';
import s from './Slide.module.css'

const Slide = (props) => {
    return (
        <div className={s.slide} style={{transform: 'translateX(' + props.translate + '%)'}}>
            {props.children}
        </div>
    )
};

export default Slide;