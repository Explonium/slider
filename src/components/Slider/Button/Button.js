import React from 'react';
import leftArrow from '../icons/left-arrow.svg'
import rightArrow from '../icons/right-arrow.svg'
import s from './Button.module.css';
import SliderContext from "../Slider/SliderContext";

const Button = (props) => {

    let img;

    switch (props.side) {
        case 'left':
            img = leftArrow;
            break;
        case 'right':
            img = rightArrow;
            break;
        default:
            img='';
    }

    return (
        <SliderContext.Consumer>
            {(slider) => (
                <button
                    className={s.button}
                    disabled={!slider.state.buttonsEnabled}
                    style={slider.getButtonStyle(props.side)}
                    onClick={() => {slider.changeSlideBy(props.side === 'left' ? -1 : 1)}}>
                    <img src={img} alt={'arrow'}/>
                </button>
            )}
        </SliderContext.Consumer>
    )
};

export default Button;