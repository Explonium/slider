import React from 'react';
import s from './Dots.module.css'
import SliderContext from "../Slider/SliderContext";

const Dots = () => {
    return (
        <SliderContext.Consumer>
            { (slider) => (
                <div className={s.dots}>
                    {slider.getDots()}
                </div>
            )}
        </SliderContext.Consumer>
    )
};

export default Dots;