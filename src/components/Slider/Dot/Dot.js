import React from 'react';
import s from './Dot.module.css';
import SliderContext from "../Slider/SliderContext";

const Dot = (props) => {

    let className = s.dot;
    if (props.active === true)
        className += " " + s.dotActive;

    return (
        <SliderContext.Consumer>
            {(slider) =>(
                <button id={props.id} className={className} onClick={slider.onDotClick}/>
            )}
        </SliderContext.Consumer>
    )
};

export default Dot;