import React from 'react';
import Slider from "./Slider";
import Slide from "../Slide/Slide";
import SliderContext from "./SliderContext";
import Dot from "../Dot/Dot";

function mod(n, m){
    return ((n % m) + m) % m;
}

class SliderContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            slider: React.createRef(),
            currentSlide: 0,        // Slide, that is currently displaying
            trStartSlide: 0,        // Slide, from which transition animation started
            trEndSlide: 0,
            transition: props.transition,
            autoScrollDelay: props.autoScrollDelay,
            gap: props.gap,

            swipeOffset: 0,
            swipeStartX: 0,
            isSwiping: false,

            buttonsShown: true,
            dotsShown: true,

            swipesEnabled: props.swipesEnabled,
            autoScrollEnabled: props.autoScrollEnabled,
            buttonsEnabled: props.buttonsEnabled,
            dotsEnabled: props.dotsEnabled
        }
    }

    // Getters ==============================================================
    getCurrentSlides() {
        const gap = 1;
        let outputItems = [];

        for (let i = this.state.trStartSlide - gap; i <= this.state.trEndSlide + gap; i++)
            outputItems.push(
                <Slide key={i} translate={(this.state.trStartSlide - gap) * 100}>
                    {this.props.children[mod(i, this.props.children.length)]}
                </Slide>
            );

        return outputItems;
    }

    getDots() {
        let dots = [];

        for (let i = 0; i < this.props.children.length; i++) {
            const active = i === mod(this.state.currentSlide, this.props.children.length);

            dots.push(<Dot active={active} key={i} id={i}/>);
        }

        return dots;
    }

    getButtonStyle(side = 'left') {
        return {
            opacity: (this.state.buttonsEnabled * this.state.buttonsShown * 50) + '%',
            gridArea: side
        }
    }

    getFrameStyle() {
        return {
            transition: this.state.transition * !this.state.isSwiping + 's ease-out',
            transform: 'translateX(' + ((-this.state.currentSlide + this.state.swipeOffset) * 100) + '%)'
        }
    }

    getSlideStyle(transform) {
        return {
            transform: 'translateX(' + transform + '%)'
        }
    }

    // Other ==============================================================

    changeSlideBy(n) {
        // n cannot be greater than amount of slides / 2.
        // It must be in range:
        // [-length/2, length]
        n = mod(n, this.props.children.length);
        if (n > this.props.children.length / 2)
            n -= this.props.children.length;

        // Moving slider by n slides
        this.setState((state, props) => ({
            trStartSlide: Math.min(state.currentSlide + n, state.trStartSlide),
            trEndSlide: Math.max(state.currentSlide + n, state.trEndSlide),

            translate: state.currentSlide - n,
            currentSlide: state.currentSlide + n
        }));
    };

    // Hooks =====================================================

    resetEndItem = () => {
        // Calls after animation
        // Setting item to endItem
        this.setState((state) => ({
            trStartSlide: state.currentSlide,
            trEndSlide: state.currentSlide
        }));
    };

    onTouchDown = (event) => {
        this.setState({
            swipeStartX: event.touches[0].clientX,
            isSwiping: this.state.swipesEnabled
        });
    };
    onMouseDown = (event) => {
        this.setState({
            swipeStartX: event.clientX,
            isSwiping: this.state.swipesEnabled
        });
    };

    onTouchMove = (event) => {
        let clientX = event.touches[0].clientX;

        this.setState((state) => ({
            swipeOffset: Math.min(Math.max((clientX - state.swipeStartX)  / state.slider.current.offsetWidth, -1), 1)
        }));
    };

    onMouseMove = (event) => {
        if (this.state.isSwiping) {
            let clientX = event.clientX;

            this.setState((state) => ({
                swipeOffset: (clientX - state.swipeStartX) / state.slider.current.offsetWidth
            }));
        }
    };

    onTouchUp = () => {
        if (this.state.swipeOffset > 0.3)
            this.changeSlideBy(-1);

        if (this.state.swipeOffset < -0.3)
            this.changeSlideBy(1);

        this.setState({
            swipeOffset: 0,
            swipeStartX: 0,
            isSwiping: false
        });
    };

    onMouseEnter = (event) => {
        if (event.buttons % 2 === 1 && this.state.isSwiping === false)
            this.setState({
                swipeStartX: event.clientX,
                isSwiping: this.state.swipesEnabled,
                buttonsShown: true
            });
    };
    onMouseOver = () => {

    };
    onMouseOut = () => {

    };

    // Buttons and dots

    onDotClick = (event) => {
        this.changeSlideBy(event.target.id - this.state.currentSlide);
    };

    render(props) {
        return (
            <SliderContext.Provider value={this}>
                <Slider slider={this.state.slider}
                        onTouchStart={this.onTouchDown} onMouseDown={this.onMouseDown}
                        onMouseLeave={this.onTouchUp} onMouseEnter={this.onMouseEnter}
                        onTouchMove={this.onTouchMove} onMouseMove={this.onMouseMove}
                        onTouchEnd={this.onTouchUp} onMouseUp={this.onTouchUp}>
                    {this.getCurrentSlides()}
                </Slider>
            </SliderContext.Provider>
        )
    }
}

SliderContainer.defaultProps = {
    transition: 0.5,
    autoScrollDelay: 5,
    gap: 0,

    swipesEnabled: true,
    autoScrollEnabled: true,
    buttonsEnabled: true,
    dotsEnabled: true,

    children: <div/>
};
export default SliderContainer;