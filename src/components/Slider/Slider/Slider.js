import React from 'react';
import SliderFrame from "../SlidesFrame/SliderFrame";
import Button from "../Button/Button";
import Dots from "../Dots/Dots";
import s from './Slider.module.css'

const Slider = (props) => {
    return (
        <div ref={props.slider} className={s.slider}
             onTouchStart={props.onTouchStart} onMouseDown={props.onMouseDown}
             onMouseLeave={props.onMouseLeave} onMouseEnter={props.onMouseEnter}
             onTouchMove={props.onTouchMove} onMouseMove={props.onMouseMove}
             onTouchEnd={props.onTouchEnd} onMouseUp={props.onMouseUp}
             onMouseOver={props.onMouseOver} onMouseOut={props.onMouseOut}>
            <SliderFrame>
                {props.children}
            </SliderFrame>
            <Button side={'left'}/>
            <Button side={'right'}/>
            <Dots/>
        </div>
    )
};

export default Slider;