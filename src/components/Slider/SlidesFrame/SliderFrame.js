import React from 'react';
import s from './SliderFrame.module.css'
import SliderContext from "../Slider/SliderContext";

const SliderFrame = (props) => {
    return (
        <SliderContext.Consumer>
            {(slider) => (
                <div className={s.sliderFrame} style={slider.getFrameStyle()} onTransitionEnd={slider.resetEndItem}>
                    {props.children}
                </div>
            )}
        </SliderContext.Consumer>
    )
};

export default SliderFrame;