import React from 'react';
import s from './App.module.css'
import Slide from "../Slider/Slide/Slide";
import SliderContainer from "../Slider/Slider/SliderContainer";

class App extends React.Component{
    render(){
        return (
            <div className={s.appWrapper}>
                <div className={s.half}>
                    <SliderContainer>
                        <div>
                            <h1 className={s.textCenter}>This is first slide!</h1>
                        </div>
                        <div>
                            <img src={'https://images.unsplash.com/photo-1579546929518-9e396f3cc809?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHw%3D&w=1000&q=80'}/>
                            <h1 className={s.textCenter}>There is can be any HTML content!</h1>
                        </div>
                        <div>
                            <img src={'https://www.freecodecamp.org/news/content/images/2020/04/w-qjCHPZbeXCQ-unsplash.jpg'}/>
                            <h1 className={s.textCenter}>As many as you want!</h1>
                        </div>
                        <div>
                            <img src={'https://coolbackgrounds.io/images/backgrounds/index/ranger-4df6c1b6.png'}/>
                            <h1 className={s.textCenter}>Infinite!</h1>
                        </div>
                    </SliderContainer>
                </div>

                <div className={s.half}>
                    <SliderContainer>
                        <div>
                            <h1 className={s.textCenter}>As many sliders as you want!</h1>
                        </div>
                        <div>
                            <img src={'https://images.unsplash.com/photo-1579546929518-9e396f3cc809?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHw%3D&w=1000&q=80'}/>
                            <h1 className={s.textCenter}>Can hold rapid clicks</h1>
                        </div>
                        <div>
                            <img src={'https://www.freecodecamp.org/news/content/images/2020/04/w-qjCHPZbeXCQ-unsplash.jpg'}/>
                            <h1 className={s.textCenter}>Supports swipes for desktop and mobile</h1>
                        </div>
                    </SliderContainer>
                </div>
            </div>
        )
    }
}

export default App;